package justeat.arkangelx.app.justeattest.ui;
/**
 * Created by arkangel on 18/05/15.
 */

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import justeat.arkangelx.app.justeat_module.builders.JustEatRestaurantsQueryRequestBuilder;
import justeat.arkangelx.app.justeat_module.logs.Log;
import justeat.arkangelx.app.justeat_module.model.Restaurant;
import justeat.arkangelx.app.justeat_module.model.RestaurantRootContainer;
import justeat.arkangelx.app.justeat_module.utils.DeviceUtils;
import justeat.arkangelx.app.justeattest.R;
import justeat.arkangelx.app.justeattest.adapters.StickyListAdapter;
import justeat.arkangelx.app.justeattest.utils.CroutonUtils;
import justeat.arkangelx.app.justeattest.utils.EventBusProvider;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.android.widget.OnTextChangeEvent;
import rx.android.widget.WidgetObservable;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import static java.lang.String.format;
import static rx.android.app.AppObservable.bindFragment;

public class RestaurantsListFragment extends Fragment implements AbsListView.OnScrollListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener {

    @InjectView(R.id.restaurant_listview) StickyListHeadersListView matchListView;
    @InjectView(android.R.id.empty) View emptyStateView;
    @InjectView(R.id.restaurant_query_edit_text) EditText inputSearchBox;
    private int bigHeight;
    private int smallHeight;
    private boolean fadeHeader = true;

    private Subscription subscription;

    private View view;
    private StickyListAdapter adapter;

    private View currentlyStickyHeader;
    private int currentlyStickyHeaderPosition;

    public static RestaurantsListFragment getInstance() {
        RestaurantsListFragment stickyHeaderMatchList = new RestaurantsListFragment();
        return stickyHeaderMatchList;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (subscription != null) {
            subscription.unsubscribe();
        }
        EventBusProvider.getInstance().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.screen_restaurant_list, container, false);
        ButterKnife.inject(this, view);
        adapter = new StickyListAdapter(getActivity());
        matchListView.setEmptyView(emptyStateView);
        matchListView.setOnStickyHeaderChangedListener(this);
        matchListView.setOnStickyHeaderOffsetChangedListener(this);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        EventBusProvider.getInstance().register(this);
        if (!DeviceUtils.isConnected(getActivity())) {
            CroutonUtils.error(getActivity(), "No internet available");
            ((TextView) view.findViewById(R.id.no_data_textView)).setText("No Internet");
            ((ProgressBar) view.findViewById(R.id.progressbar)).setVisibility(View.GONE);
            matchListView.setEmptyView(emptyStateView);
            inputSearchBox.setEnabled(false);
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Observable<OnTextChangeEvent> textChangeObservable = WidgetObservable.text(inputSearchBox);
        subscription = bindFragment(this,//
                textChangeObservable//
                        .debounce(400, TimeUnit.MILLISECONDS)// default Scheduler is Computation
                        .observeOn(AndroidSchedulers.mainThread()))//
                .subscribe(getSearchObserver());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {
        if (fadeHeader && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setAlpha(1);
        currentlyStickyHeader = header;
        currentlyStickyHeaderPosition = l.getFirstVisiblePosition();
        setStickyHeaderHeight(bigHeight);
    }

    public void setStickyHeaderHeight(int stickyHeaderHeight) {
        ViewGroup.LayoutParams lp = currentlyStickyHeader.getLayoutParams();
        lp.height = stickyHeaderHeight;
        currentlyStickyHeader.setLayoutParams(lp);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // don't care
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (currentlyStickyHeader == null || currentlyStickyHeaderPosition != firstVisibleItem) {
            return;
        }
        int height = bigHeight + view.getChildAt(0).getTop();
        if (height < smallHeight) {
            height = smallHeight;
        }
        setStickyHeaderHeight(height);
    }

    private Observer<OnTextChangeEvent> getSearchObserver() {
        return new Observer<OnTextChangeEvent>() {
            @Override
            public void onCompleted() {
                Log.i("Requst completed successfully");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(e.getMessage());
            }

            @Override
            public void onNext(OnTextChangeEvent onTextChangeEvent) {
                Log.i(format("Searching for %s", onTextChangeEvent.text().toString()));
                ((ProgressBar) view.findViewById(R.id.progressbar)).setVisibility(View.VISIBLE);
                if (DeviceUtils.isConnected(getActivity())) {
                    JustEatRestaurantsQueryRequestBuilder.newBuilder(onTextChangeEvent.text().toString()).setResponseListener(new Response.Listener<RestaurantRootContainer>() {
                        @Override
                        public void onResponse(RestaurantRootContainer rootContainer) {
                            if (null != rootContainer && !rootContainer.getRestaurants().isEmpty()) {
                                adapter.addAll((ArrayList<Restaurant>) rootContainer.getRestaurants());
                                matchListView.setAdapter(adapter);
                                Toast.makeText(getActivity(), "Finished Request", Toast.LENGTH_SHORT).show();
                                justeat.arkangelx.app.justeat_module.logs.Log.i(RestaurantsListFragment.class.getSimpleName(), null);
                            }
                        }
                    }, RestaurantRootContainer.class).setErrorListener(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            justeat.arkangelx.app.justeat_module.logs.Log.i(RestaurantsListFragment.class.getSimpleName(), null);
                        }
                    }).allowCache(true).
                            setTag(RestaurantsListFragment.class.getSimpleName().toString()).build().execute();
                } else {
                    CroutonUtils.error(getActivity(), "No internet available");
                    ((TextView) view.findViewById(R.id.no_data_textView)).setText("No Internet");
                    ((ProgressBar) view.findViewById(R.id.progressbar)).setVisibility(View.GONE);
                    matchListView.setEmptyView(emptyStateView);
                }
            }
        };
    }

    @Subscribe
    public void requestRestaurantsWithUsersPostcode(LocationUpdateEvent locationUpdateEvent) {
        if (locationUpdateEvent != null) {
            inputSearchBox.setText("");
            inputSearchBox.append(locationUpdateEvent.getPartPostCode());
        }
    }

    public static class LocationUpdateEvent {

        private String partPostCode;

        public String getPartPostCode() {
            return partPostCode;
        }

        public void setPartPostCode(String partPostCode) {
            this.partPostCode = partPostCode;
        }
    }
}
