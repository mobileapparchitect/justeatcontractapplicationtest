package justeat.arkangelx.app.justeattest;

import justeat.arkangelx.app.justeat_module.JustEatApplication;

public class JustEatMainApplication extends JustEatApplication {

    private static JustEatMainApplication singleton;

    public static JustEatMainApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}