package justeat.arkangelx.app.justeattest.location_services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import justeat.arkangelx.app.justeattest.ui.RestaurantsListFragment;
import justeat.arkangelx.app.justeattest.utils.CroutonUtils;
import justeat.arkangelx.app.justeattest.utils.EventBusProvider;

/**
 * this class was inserted from an old project i worked on for a private client.
 * <p/>
 * I have lots of comments that aren't necessary and i would write it differently
 * https://bitbucket.org/android_god/cft-portal-app/src/3f2e329ccf10f6c92ed606a9981fbf932f2738e4/src/com/mobileparadigm/arkangelx/cftportal/location_misc/LocationFinder.java?at=default
 */
public class LocationFinder implements LocationListener {

    // Keys for maintaining UI states after rotation.
    private static final String KEY_FINE = "use_fine";
    private static final String KEY_BOTH = "use_both";
    // UI handler codes.
    private static final int UPDATE_ADDRESS = 1;
    private static final int UPDATE_LATLNG = 2;
    private static final int TEN_SECONDS = 10000;
    private static final int TEN_METERS = 10;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private Activity mActivity;

    private LocationManager mLocationManager;

    private boolean mGeocoderAvailable;
    private boolean mUseFine;

    private boolean mUseBoth;

    public LocationFinder(Activity activity) {
        mActivity = activity;
        mUseBoth = true;
        // The isPresent() helper method is only available on Gingerbread or
        // above.
        mGeocoderAvailable = Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD
                && Geocoder.isPresent();
        // Get a reference to the LocationManager object.
        mLocationManager = (LocationManager) mActivity
                .getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = mLocationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            CroutonUtils.error(mActivity, "Please enable location services on your phone!");
            enableLocationSettings();
        }
        setup();
    }

    private void setup() {
        Location gpsLocation = null;
        Location networkLocation = null;
        mLocationManager.removeUpdates(LocationFinder.this);
        // Get fine location updates only.
        if (mUseFine) {
            gpsLocation = requestUpdatesFromProvider(LocationManager.GPS_PROVIDER, 0);
            // Update the UI immediately if a location is obtained.
            if (gpsLocation != null)
                updateUILocation(gpsLocation);
        } else if (mUseBoth) {
            gpsLocation = requestUpdatesFromProvider(LocationManager.GPS_PROVIDER, 0);
            networkLocation = requestUpdatesFromProvider(LocationManager.NETWORK_PROVIDER,
                    0);
            // If both providers return last known locations, compare the two
            // and use the better
            // one to update the UI. If only one provider returns a location,
            // use it.
            if (gpsLocation != null && networkLocation != null) {
                updateUILocation(getBetterLocation(gpsLocation, networkLocation));
            } else if (gpsLocation != null) {
                updateUILocation(gpsLocation);
            } else if (networkLocation != null) {
                updateUILocation(networkLocation);
            }
        }
    }

    private Location requestUpdatesFromProvider(final String provider,
                                                final int errorResId) {
        Location location = null;
        if (mLocationManager.isProviderEnabled(provider)) {
            mLocationManager.requestLocationUpdates(provider, TEN_SECONDS,
                    TEN_METERS, this);
            location = mLocationManager.getLastKnownLocation(provider);
        } else {
            //Toast.makeText(this, errorResId, Toast.LENGTH_LONG).show();
        }
        return location;
    }

    // Method to launch Settings
    private void enableLocationSettings() {
        Intent settingsIntent = new Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        mActivity.startActivity(settingsIntent);
    }

    private void doReverseGeocoding(Location location) {
        (new ReverseGeocodingTask(mActivity))
                .execute(new Location[]{location});
    }

    public void start() {
        LocationManager locationManager = (LocationManager) mActivity
                .getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void updateUILocation(Location location) {
        // We're sending the update to a handler which then updates the UI with
        // the new
        // location.
       /* Message.obtain(mHandler, UPDATE_LATLNG,
                location.getLatitude() + ", " + location.getLongitude())
                .sendToTarget();*/
        // Bypass reverse-geocoding only if the Geocoder service is available on
        // the device.
        if (mGeocoderAvailable)
            doReverseGeocoding(location);
    }

    protected Location getBetterLocation(Location newLocation,
                                         Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return newLocation;
        }
        // Check whether the new location fix is newer or older
        long timeDelta = newLocation.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;
        // If it's been more than two minutes since the current location, use
        // the new location
        // because the user has likely moved.
        if (isSignificantlyNewer) {
            return newLocation;
            // If the new location is more than two minutes older, it must be
            // worse
        } else if (isSignificantlyOlder) {
            return currentBestLocation;
        }
        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (newLocation.getAccuracy() - currentBestLocation
                .getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(newLocation.getProvider(),
                currentBestLocation.getProvider());
        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return newLocation;
        } else if (isNewer && !isLessAccurate) {
            return newLocation;
        } else if (isNewer && !isSignificantlyLessAccurate
                && isFromSameProvider) {
            return newLocation;
        }
        return currentBestLocation;
    }

    // Callback method for the "fine provider" button.
    public void useFineProvider(View v) {
        mUseFine = true;
        mUseBoth = false;
        setup();
    }

    // Callback method for the "both providers" button.
    public void useCoarseFineProviders(View v) {
        mUseFine = false;
        mUseBoth = true;
        setup();
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onLocationChanged(Location location) {
        updateUILocation(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    // AsyncTask encapsulating the reverse-geocoding API. Since the geocoder API
    // is blocked,
    // we do not want to invoke it from the UI thread.
    private class ReverseGeocodingTask extends AsyncTask<Location, Void, Void> {

        Context mContext;
        List<Address> x;

        public ReverseGeocodingTask(Context context) {
            super();
            mContext = context;
        }

        private double distance(double lat1, double lon1, double lat2,
                                double lon2) {
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                    + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
                    * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            return (dist);
        }

        private double deg2rad(double deg) {
            return (deg * Math.PI / 180.0);
        }

        private double rad2deg(double rad) {
            return (rad * 180.0 / Math.PI);
        }

        @Override
        protected Void doInBackground(Location... params) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            Location loc = params[0];
            List<Address> addresses = null;
            double dist = 0.0;
            try {
                addresses = geocoder.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);
                x = geocoder.getFromLocationName("SE2 9QA", 1);
                dist = distance(addresses.get(0).getLatitude(), addresses
                        .get(0).getLongitude(), x.get(0).getLatitude(), x
                        .get(0).getLongitude());
            } catch (IOException e) {
                e.printStackTrace();
                // Update address field with the exception.
          /*      Message.obtain(mHandler, UPDATE_ADDRESS, e.toString())
                        .sendToTarget();*/
            }
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                // Format the first line of address (if available), city, and
                // country name.
                String addressText = String.format(
                        "%s, %s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address
                                .getAddressLine(0) : "", address.getLocality(),
                        address.getAdminArea(), address.getCountryName());
                // Update address field on UI.
                if (!TextUtils.isEmpty(address.getPostalCode())) {
                    String[] postCodeParts = address.getPostalCode().split(" ");
                    RestaurantsListFragment.LocationUpdateEvent locationUpdateEvent = new RestaurantsListFragment.LocationUpdateEvent();
                    locationUpdateEvent.setPartPostCode(postCodeParts[0]);
                    EventBusProvider.getInstance().post(locationUpdateEvent);

                }
            }
            return null;
        }
    }
}