package justeat.arkangelx.app.justeattest.utils;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public final class EventBusProvider {

    private static final GolCamBus bus = new GolCamBus(ThreadEnforcer.ANY);

    private EventBusProvider() {
    }

    public static Bus getInstance() {
        return bus;
    }

    private static class GolCamBus extends Bus {

        private final Handler mHandler = new Handler(Looper.getMainLooper());

        public GolCamBus(ThreadEnforcer main) {
            super(main);
        }

        @Override
        public void post(final Object event) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                super.post(event);
            } else {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        GolCamBus.super.post(event);
                    }
                });
            }
        }

    }
}