package justeat.arkangelx.app.justeattest.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import justeat.arkangelx.app.justeat_module.model.CuisineType;
import justeat.arkangelx.app.justeat_module.model.Restaurant;
import justeat.arkangelx.app.justeattest.R;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class StickyListAdapter extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {

    private Context mContext;

    private LayoutInflater mInflater;

    private Character[] rows;
    private int[] mSectionIndices;
    private ArrayList<Long> listOfheaders;
    private ArrayList<Restaurant> allRestaurants;

    public StickyListAdapter(Context context) {
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
        allRestaurants = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return allRestaurants.size();
    }

    @Override
    public Object getItem(int position) {
        return allRestaurants.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void allRestaurantNamesToUpperCase() {
        for (Restaurant restaurant : allRestaurants) {
            restaurant.setName(restaurant.getName().toUpperCase());
        }
    }

    public void addAll(ArrayList<Restaurant> items) {
        allRestaurants = items;
        allRestaurantNamesToUpperCase();
        mSectionIndices = getSectionIndices();
        rows = getSectionLetters();
        listOfheaders = new ArrayList<>();
        setUpHeaders(allRestaurants);
        notifyDataSetChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void setUpHeaders(ArrayList<Restaurant> parseObjectArrayList) {
        for (int count = 0; count < parseObjectArrayList.size(); count++) {
            Restaurant restaurant = (Restaurant) parseObjectArrayList.get(count);
            long hashCode = (long) restaurant.getName().hashCode();
            listOfheaders.add(hashCode);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.restaurant_row, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.restaurant_name);
            holder.rating = (TextView) convertView.findViewById(R.id.restaurant_rating);
            holder.titleOne = (TextView) convertView.findViewById(R.id.cuisineTitleOne);
            holder.titleTwo = (TextView) convertView.findViewById(R.id.cuisineTitleTwo);
            holder.cuisineTitleOne = (TextView) convertView.findViewById(R.id.cuisineRowOne);
            holder.cuisineTitleTwo = (TextView) convertView.findViewById(R.id.cuisineRowTwo);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Restaurant restaurant = (Restaurant) getItem(position);
        holder.name.setText(restaurant.getName());
        holder.rating.setText(String.valueOf(restaurant.getNumberOfRating()));
        if (!TextUtils.isEmpty(((Restaurant) getItem(position)).getLogos().get(0).getStandardResolutionURL())) {
            Glide.with(mContext)
                    .load(((Restaurant) getItem(position)).getLogos().get(0).getStandardResolutionURL())
                    .into(holder.imageView);
        }
        ArrayList<CuisineType> cuisineTypes = (ArrayList<CuisineType>) restaurant.getCuisineTypes();
        if (!cuisineTypes.isEmpty()) {
            switch (cuisineTypes.size()) {
                case 1:
                    holder.titleOne.setVisibility(View.VISIBLE);
                    holder.cuisineTitleOne.setVisibility(View.VISIBLE);
                    holder.cuisineTitleOne.setText(cuisineTypes.get(0).getName());
                    break;
                case 2:
                    holder.titleOne.setVisibility(View.VISIBLE);
                    holder.cuisineTitleOne.setVisibility(View.VISIBLE);
                    holder.cuisineTitleOne.setText(cuisineTypes.get(0).getName());
                    holder.titleTwo.setVisibility(View.VISIBLE);
                    holder.cuisineTitleTwo.setVisibility(View.VISIBLE);
                    holder.cuisineTitleTwo.setText(cuisineTypes.get(1).getName());
                    break;
            }
        }
        return convertView;
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = ((Restaurant) getItem(0)).getName().charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < allRestaurants.size(); i++) {
            if (allRestaurants.get(i).getName().charAt(0) != lastFirstChar) {
                lastFirstChar = allRestaurants.get(i).getName().charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return null;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup parent) {
        HeaderViewHolder headerViewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.header_layout, parent, false);
            headerViewHolder = new HeaderViewHolder();
            headerViewHolder.headerView = (TextView) convertView.findViewById(R.id.header);
            headerViewHolder.headerView.setClickable(false);
            convertView.setTag(headerViewHolder);
        } else {
            headerViewHolder = (HeaderViewHolder) convertView.getTag();
        }
        CharSequence headerChar = allRestaurants.get(i).getName().subSequence(0, 1);
        headerViewHolder.headerView.setText(headerChar);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return listOfheaders.get(position);
    }

    @Override
    public Object[] getSections() {
        return rows;
    }

    @Override
    public int getPositionForSection(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    private Character[] getSectionLetters() {
        if (mSectionIndices == null) {
            mSectionIndices = new int[1];
        }
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = allRestaurants.get(i).getName().charAt(0);
        }
        return letters;
    }

    @Override
    public int getSectionForPosition(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }
        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    public class ViewHolder {

        TextView name;
        TextView rating;
        ImageView imageView;
        TextView cuisineTitleOne;
        TextView titleOne, titleTwo;
        TextView cuisineTitleTwo;
        LinearLayout cuisineTypes;
    }

    public class HeaderViewHolder {

        TextView headerView;
    }
}