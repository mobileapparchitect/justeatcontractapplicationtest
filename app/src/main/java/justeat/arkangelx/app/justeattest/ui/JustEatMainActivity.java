package justeat.arkangelx.app.justeattest.ui;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import justeat.arkangelx.app.justeattest.R;
import justeat.arkangelx.app.justeattest.location_services.LocationFinder;

public class JustEatMainActivity extends FragmentActivity {

    public Button locationButton;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setCustomActionBar(true);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_container, RestaurantsListFragment.getInstance()).commit();
    }



    public void setCustomActionBar(boolean showFlag) {
        if (showFlag) {
            getActionBar().show();
            final LayoutInflater mInflater = LayoutInflater.from(this);
            final View customView = mInflater.inflate(R.layout.custom_actionbar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ActionBar actionBar = getActionBar();
            actionBar.setCustomView(customView, params);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(false);
            ((Button) customView.findViewById(R.id.locationButton)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocationFinder finder = new LocationFinder(JustEatMainActivity.this);
                }
            });
        } else {
            getActionBar().hide();
        }
    }
}
