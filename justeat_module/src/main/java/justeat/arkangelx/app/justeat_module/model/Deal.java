package justeat.arkangelx.app.justeat_module.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Deal implements Parcelable {

    public static final Creator<Deal> CREATOR = new Creator<Deal>() {
        public Deal createFromParcel(Parcel in) {
            return new Deal(in);
        }

        public Deal[] newArray(int size) {
            return new Deal[size];
        }
    };
    @JsonProperty("DiscountPercent")
    private int mDiscountPercent;
    @JsonProperty("QualifyingPrice")
    private double mQualifyingPrice;
    @JsonProperty("Description")
    private String mDescription;

    public Deal() {
    }

    public Deal(Parcel in) {
        mDiscountPercent = in.readInt();
        mQualifyingPrice = in.readDouble();
        mDescription = in.readString();
    }

    public int getDiscountPercent() {
        return mDiscountPercent;
    }

    public void setDiscountPercent(int discountPercent) {
        mDiscountPercent = discountPercent;
    }

    public double getQualifyingPrice() {
        return mQualifyingPrice;
    }

    public void setQualifyingPrice(double qualifyingPrice) {
        mQualifyingPrice = qualifyingPrice;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mDiscountPercent);
        dest.writeDouble(mQualifyingPrice);
        dest.writeString(mDescription);
    }
}