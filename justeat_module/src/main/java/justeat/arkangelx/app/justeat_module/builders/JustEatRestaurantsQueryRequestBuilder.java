package justeat.arkangelx.app.justeat_module.builders;

import org.apache.http.message.BasicNameValuePair;

public class JustEatRestaurantsQueryRequestBuilder extends GetRequestBuilder {

    public JustEatRestaurantsQueryRequestBuilder(String outCode) {
        super();
        params.add(new BasicNameValuePair(RequestFields.FIELD_OUTCODE, outCode));
        url += RequestMethods.METHOD_RESTAURANTS;
    }

    public static JustEatRestaurantsQueryRequestBuilder newBuilder(String outCode) {
        return new JustEatRestaurantsQueryRequestBuilder(outCode);
    }
}
