package justeat.arkangelx.app.justeat_module.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Logo implements Parcelable {

    public static final Creator<Logo> CREATOR = new Creator<Logo>() {
        public Logo createFromParcel(Parcel in) {
            return new Logo(in);
        }

        public Logo[] newArray(int size) {
            return new Logo[size];
        }
    };
    @JsonProperty("StandardResolutionURL")
    private String mStandardResolutionURL;

    public Logo() {
    }

    public Logo(Parcel in) {
        mStandardResolutionURL = in.readString();
    }

    public String getStandardResolutionURL() {
        return mStandardResolutionURL;
    }

    public void setStandardResolutionURL(String standardResolutionURL) {
        mStandardResolutionURL = standardResolutionURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mStandardResolutionURL);
    }
}