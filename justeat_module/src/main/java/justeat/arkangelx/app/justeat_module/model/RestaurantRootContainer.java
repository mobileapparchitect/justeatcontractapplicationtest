package justeat.arkangelx.app.justeat_module.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestaurantRootContainer implements Parcelable {

    public static final Creator<RestaurantRootContainer> CREATOR = new Creator<RestaurantRootContainer>() {
        public RestaurantRootContainer createFromParcel(Parcel in) {
            return new RestaurantRootContainer(in);
        }

        public RestaurantRootContainer[] newArray(int size) {
            return new RestaurantRootContainer[size];
        }
    };
    @JsonProperty("ShortResultText")
    private String mShortResultText;
    @JsonProperty("Restaurants")
    private List<Restaurant> mRestaurants;

    public RestaurantRootContainer() {
    }

    public RestaurantRootContainer(Parcel in) {
        mShortResultText = in.readString();
        mRestaurants = new ArrayList<Restaurant>();
        in.readTypedList(mRestaurants, Restaurant.CREATOR);
    }

    public String getShortResultText() {
        return mShortResultText;
    }

    public void setShortResultText(String shortResultText) {
        mShortResultText = shortResultText;
    }

    public List<Restaurant> getRestaurants() {
        return mRestaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        mRestaurants = restaurants;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mShortResultText);
        dest.writeTypedList(mRestaurants);
    }
}