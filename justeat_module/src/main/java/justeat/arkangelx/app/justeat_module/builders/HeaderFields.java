package justeat.arkangelx.app.justeat_module.builders;


public interface HeaderFields {

    public static final String AUTHORIZATION = "Authorization";
    public static final String ENCODING_GZIP = "gzip";
    public static final String CONTENT_ENCODING = "Content-Encoding";

    public static final String Host = "Host";
    public static final String AcceptTenant = "Accept-Tenant";
    public static final String AcceptLanguage ="Accept-Language";
}
