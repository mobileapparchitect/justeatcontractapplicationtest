package justeat.arkangelx.app.justeat_module.model;

import java.util.ArrayList;
import android.os.Parcelable;
import java.util.List;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Restaurant implements Parcelable{


    @JsonProperty("Postcode")
    private String mPostcode;

    @JsonProperty("Logo")
    private List<Logo> mLogos;

    @JsonProperty("ReasonWhyTemporarilyOffline")
    private String mReasonWhyTemporarilyOffline;

    @JsonProperty("IsCloseBy")
    private boolean mIsCloseBy;

    @JsonProperty("Deals")
    private List<Deal> mDeals;

    @JsonProperty("IsNew")
    private boolean mIsNew;
    @JsonProperty("IsOpenNowForDelivery")
    private boolean mIsOpenNowForDelivery;

    @JsonProperty("IsTemporarilyOffline")
    private boolean mIsTemporarilyOffline;

    @JsonProperty("Id")
    private long mId;

    @JsonProperty("City")
    private String mCity;

    @JsonProperty("CuisineTypes")
    private List<CuisineType> mCuisineTypes;

    @JsonProperty("IsOpenNow")
    private boolean mIsOpenNow;

    @JsonProperty("UniqueName")
    private String mUniqueName;


    @JsonProperty("IsOpenNowForCollection")
    private boolean mIsOpenNowForCollection;

    @JsonProperty("Url")
    private String mUrl;

    @JsonProperty("DefaultDisplayRank")
    private int mDefaultDisplayRank;

    @JsonProperty("IsHalal")
    private boolean mIsHalal;

    @JsonProperty("Name")
    private String mName;

    @JsonProperty("IsSponsored")
    private boolean mIsSponsored;

    @JsonProperty("NumberOfRatings")
    private int mNumberOfRating;

    @JsonProperty("Address")
    private String mAddress;

    @JsonProperty("RatingStars")
    private double mRatingStar;


    public Restaurant(){

    }

    public void setPostcode(String postcode) {
        mPostcode = postcode;
    }

    public String getPostcode() {
        return mPostcode;
    }

    public void setLogos(List<Logo> logos) {
        mLogos = logos;
    }

    public List<Logo> getLogos() {
        return mLogos;
    }

    public void setReasonWhyTemporarilyOffline(String reasonWhyTemporarilyOffline) {
        mReasonWhyTemporarilyOffline = reasonWhyTemporarilyOffline;
    }

    public String getReasonWhyTemporarilyOffline() {
        return mReasonWhyTemporarilyOffline;
    }

    public void setIsCloseBy(boolean isCloseBy) {
        mIsCloseBy = isCloseBy;
    }

    public boolean isIsCloseBy() {
        return mIsCloseBy;
    }

    public void setDeals(List<Deal> deals) {
        mDeals = deals;
    }

    public List<Deal> getDeals() {
        return mDeals;
    }

    public void setIsNew(boolean isNew) {
        mIsNew = isNew;
    }

    public boolean isIsNew() {
        return mIsNew;
    }

    public void setIsOpenNowForDelivery(boolean isOpenNowForDelivery) {
        mIsOpenNowForDelivery = isOpenNowForDelivery;
    }

    public boolean isIsOpenNowForDelivery() {
        return mIsOpenNowForDelivery;
    }

    public void setIsTemporarilyOffline(boolean isTemporarilyOffline) {
        mIsTemporarilyOffline = isTemporarilyOffline;
    }

    public boolean isIsTemporarilyOffline() {
        return mIsTemporarilyOffline;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCity() {
        return mCity;
    }

    public void setCuisineTypes(List<CuisineType> cuisineTypes) {
        mCuisineTypes = cuisineTypes;
    }

    public List<CuisineType> getCuisineTypes() {
        return mCuisineTypes;
    }

    public void setIsOpenNow(boolean isOpenNow) {
        mIsOpenNow = isOpenNow;
    }

    public boolean isIsOpenNow() {
        return mIsOpenNow;
    }

    public void setUniqueName(String uniqueName) {
        mUniqueName = uniqueName;
    }

    public String getUniqueName() {
        return mUniqueName;
    }

    public void setIsOpenNowForCollection(boolean isOpenNowForCollection) {
        mIsOpenNowForCollection = isOpenNowForCollection;
    }

    public boolean isIsOpenNowForCollection() {
        return mIsOpenNowForCollection;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setDefaultDisplayRank(int defaultDisplayRank) {
        mDefaultDisplayRank = defaultDisplayRank;
    }

    public int getDefaultDisplayRank() {
        return mDefaultDisplayRank;
    }

    public void setIsHalal(boolean isHalal) {
        mIsHalal = isHalal;
    }

    public boolean isIsHalal() {
        return mIsHalal;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setIsSponsored(boolean isSponsored) {
        mIsSponsored = isSponsored;
    }

    public boolean isIsSponsored() {
        return mIsSponsored;
    }

    public void setNumberOfRating(int numberOfRating) {
        mNumberOfRating = numberOfRating;
    }

    public int getNumberOfRating() {
        return mNumberOfRating;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setRatingStar(double ratingStar) {
        mRatingStar = ratingStar;
    }

    public double getRatingStar() {
        return mRatingStar;
    }

    @Override
    public boolean equals(Object obj){
        if(obj instanceof Restaurant){
            return ((Restaurant) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode(){
        return ((Long)mId).hashCode();
    }

    public Restaurant(Parcel in) {
        mPostcode = in.readString();
        mLogos = new ArrayList<Logo>();
        in.readTypedList(mLogos, Logo.CREATOR);
        mReasonWhyTemporarilyOffline = in.readString();
        mIsCloseBy = in.readInt() == 1 ? true: false;
        mDeals = new ArrayList<Deal>();
        in.readTypedList(mDeals, Deal.CREATOR);
        mIsNew = in.readInt() == 1 ? true: false;
        mIsOpenNowForDelivery = in.readInt() == 1 ? true: false;
        mIsTemporarilyOffline = in.readInt() == 1 ? true: false;
        mId = in.readLong();
        mCity = in.readString();
        mCuisineTypes = new ArrayList<CuisineType>();
        in.readTypedList(mCuisineTypes, CuisineType.CREATOR);
        mIsOpenNow = in.readInt() == 1 ? true: false;
        mUniqueName = in.readString();
        mIsOpenNowForCollection = in.readInt() == 1 ? true: false;
        mUrl = in.readString();
        mDefaultDisplayRank = in.readInt();
        mIsHalal = in.readInt() == 1 ? true: false;
        mName = in.readString();
        mIsSponsored = in.readInt() == 1 ? true: false;
        mNumberOfRating = in.readInt();
        mAddress = in.readString();
        mRatingStar = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        public Restaurant[] newArray(int size) {
        return new Restaurant[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPostcode);
        dest.writeTypedList(mLogos);
        dest.writeString(mReasonWhyTemporarilyOffline);
        dest.writeInt(mIsCloseBy ? 1 : 0);
        dest.writeTypedList(mDeals);
        dest.writeInt(mIsNew ? 1 : 0);
        dest.writeInt(mIsOpenNowForDelivery ? 1 : 0);
        dest.writeInt(mIsTemporarilyOffline ? 1 : 0);
        dest.writeLong(mId);
        dest.writeString(mCity);
        dest.writeTypedList(mCuisineTypes);
        dest.writeInt(mIsOpenNow ? 1 : 0);
        dest.writeString(mUniqueName);
        dest.writeInt(mIsOpenNowForCollection ? 1 : 0);
        dest.writeString(mUrl);
        dest.writeInt(mDefaultDisplayRank);
        dest.writeInt(mIsHalal ? 1 : 0);
        dest.writeString(mName);
        dest.writeInt(mIsSponsored ? 1 : 0);
        dest.writeInt(mNumberOfRating);
        dest.writeString(mAddress);
        dest.writeDouble(mRatingStar);
    }


}