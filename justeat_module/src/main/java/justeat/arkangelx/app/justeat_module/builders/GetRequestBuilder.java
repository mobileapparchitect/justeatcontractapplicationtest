package justeat.arkangelx.app.justeat_module.builders;




import com.android.volley.Request;

import justeat.arkangelx.app.justeat_module.network.ServicesUtils;

public  class GetRequestBuilder extends BaseRequestBuilder {
        public GetRequestBuilder() {
            setMethod(Request.Method.GET);
        }

        @Override
        public JustEatApiRequest build() {
            encodeParams();
            checkPreconditions();
            return new JustEatApiRequest(url, method, listener, responseType, errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);
        }
    }