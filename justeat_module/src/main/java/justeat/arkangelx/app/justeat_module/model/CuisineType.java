package justeat.arkangelx.app.justeat_module.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CuisineType implements Parcelable {

    public static final Creator<CuisineType> CREATOR = new Creator<CuisineType>() {
        public CuisineType createFromParcel(Parcel in) {
            return new CuisineType(in);
        }

        public CuisineType[] newArray(int size) {
            return new CuisineType[size];
        }
    };
    @JsonProperty("Id")
    private long mId;
    @JsonProperty("SeoName")
    private String mSeoName;
    @JsonProperty("Name")
    private String mName;

    public CuisineType() {
    }

    public CuisineType(Parcel in) {
        mId = in.readLong();
        mSeoName = in.readString();
        mName = in.readString();
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getSeoName() {
        return mSeoName;
    }

    public void setSeoName(String seoName) {
        mSeoName = seoName;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CuisineType) {
            return ((CuisineType) obj).getId() == mId;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return ((Long) mId).hashCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mSeoName);
        dest.writeString(mName);
    }
}