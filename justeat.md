# Just Eat

How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.


  - I spent about 5 hours on the test
  - I would improve the sticky list header UI component I tried to complete.
  


What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.

> Java Overrides Annotation
 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setCustomActionBar(true);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_container, RestaurantsListFragment.getInstance()).commit();
    }
How would you track down a performance issue in an application? Have you ever had to do this?



How would you improve the JUST EAT APIs that you just used?
 I would limit the number of json objects contained in the responses returned for the restaurants api. I can always request for more data if I need to.
 
 
 
 Please describe yourself using JSON.
 
 

